<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////// NIGHTLIFE TOI DISPLAYER - 10.08.2015 - Lucas Arundell ///////////////////////////////////////
/*

This file is basically the ‘renderer’ for Text Over Image slides. 
It accepts PHP $_POST variables given to it, then parses them into some JS
that JS is then rendered out to HTML.

The way that the BUILD process works is that it runs this file ‘virtually’ on the server, 
and the php function ob_start();  processes the renderer, then exports an HTML file.

*/

/*
Parameters of a TOI slide - passed in via $_POST

$fileName

$textBoxCount
$textLine0, $textLine1, $textLine2, $textLine3, $textLine4, $textLine5

$fontFamily
$textSize
$textColour
$bold
$shadowed

$verticalOffsetPercent
$verticalSpacingPercent

$bgColor
$bgOpacity

$is_local_image // During the build process, if we are converting a legacy toi, we tell the renderer to simply reference the same N:/ location
$bgImage

*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// GET TEXT BOX VALUES AND FILENAME
	$fileName = isset($_POST['filename']) && ($_POST['filename'] != '') ? $_POST['filename'] : '';
	$textBoxCount = isset($_POST['textBoxCount']) && ($_POST['textBoxCount'] != '') ? $_POST['textBoxCount'] : '1';
	$textLine0 = isset($_POST['tl0']) && ($_POST['tl0'] != '') ? $_POST['tl0'] : '';
	$textLine1 = isset($_POST['tl1']) && ($_POST['tl1'] != '') ? $_POST['tl1'] : '';
	$textLine2 = isset($_POST['tl2']) && ($_POST['tl2'] != '') ? $_POST['tl2'] : '';
	$textLine3 = isset($_POST['tl3']) && ($_POST['tl3'] != '') ? $_POST['tl3'] : '';
	$textLine4 = isset($_POST['tl4']) && ($_POST['tl4'] != '') ? $_POST['tl4'] : '';
	$textLine5 = isset($_POST['tl5']) && ($_POST['tl5'] != '') ? $_POST['tl5'] : '';

// CONFIGURE FONT STYLES
	$fontFamily =  isset($_POST['fontFamily']) && ($_POST['fontFamily'] != '') ? $_POST['fontFamily'] : 'open_sanslight'; 
	$textSize =  isset($_POST['textSize']) && ($_POST['textSize'] != '') ? $_POST['textSize'] : '4'; //text size in EM's based on 12px body base font size
	$textColour = isset($_POST['textColour']) && ($_POST['textColour'] != '') ? $_POST['textColour'] : '#fff';
	$bold =  isset($_POST['bold']) && ($_POST['bold'] != '') ? $_POST['bold'] : 'false'; //bold 
	$shadowed =  isset($_POST['shadowed']) && ($_POST['shadowed'] != '') ? $_POST['shadowed'] : 'false'; //shadowed 	

// CONFIGURE TEXT BOX POSITIONING
	$verticalOffsetPercent =  isset($_POST['verticalOffsetPercent']) && ($_POST['verticalOffsetPercent'] != '') ? $_POST['verticalOffsetPercent'] : '0'; //default 0
	$verticalSpacingPercent =  isset($_POST['verticalSpacingPercent']) && ($_POST['verticalSpacingPercent'] != '') ? $_POST['verticalSpacingPercent'] : '0'; //default 0


// CONFIGURE BACKGROUND DISPLAY
	$bgColor = isset($_POST['bgColor']) && ($_POST['bgColor'] != '') ? $_POST['bgColor'] : '#fff';
	$bgOpacity = isset($_POST['bgOpacity']) && ($_POST['bgOpacity'] != '') ? $_POST['bgOpacity'] : '1';
	
	$is_local_image = isset($_POST['copy_bg_img']) && ($_POST['copy_bg_img'] != '') ? $_POST['copy_bg_img'] : 'false';
	
// MAXIMUM ALLOWABLE FONT SIZE
	if($textSize > 6)
		$textSize = 6; 

// Original JPG was from a SWF TOI on the system, so just reference the N:/ reference
	if($is_local_image == 'false'){
		//$bgImage = isset($_POST['bgImage']) && ($_POST['bgImage'] != '') ? $_POST['bgImage'] : 'default_background';
		$bgImage = "file:///Z:/MARKETING/chalk_BG.jpg";
	}else{
		$bgImage = 'files/bg.jpg';
	}
?>


<html>
<head>
	<title> <?php echo $fileName . '.html'; ?> </title>
	
	<script src="js/jquery.min.js"></script>
	<link href='font-css/<?php echo $fontFamily . ".css";?>' rel='stylesheet' type='text/css'>

	<script>	
	/////////////////// SET UP VARIABLES /////////////////////
	// This passes all our gathered PHP POST values into JS //
	
	var debug = 0;  //this kills IE because all our console.logs depend on if(debug)

	var dimens={
		height: 0,
		width: 10,
		fontSize: <?php echo $textSize; ?>
	}

	var tbArray = {
		textLine0 : unescape('<?php echo $textLine0; ?>'),
		textLine1 : unescape('<?php echo $textLine1 ?>'),
		textLine2 : unescape('<?php echo $textLine2; ?>'),
		textLine3 : unescape('<?php echo $textLine3; ?>'),
		textLine4 : unescape('<?php echo $textLine4; ?>'),
		textLine5 : unescape('<?php echo $textLine5; ?>')	
	}
	
	var slideProps = {
		textBoxCount : '<?php echo $textBoxCount; ?>',			
		textSize : '<?php echo $textSize; ?>',
		textColour : '<?php echo $textColour; ?>',
		fontFamily : '<?php echo $fontFamily; ?>',
		bold : '<?php echo $bold; ?>',
		shadowed : '<?php echo $shadowed; ?>',
		verticalOffsetPercent : '<?php echo $verticalOffsetPercent; ?>',
		verticalSpacingPercent : '<?php echo $verticalSpacingPercent; ?>',
		bgColor : '<?php echo $bgColor; ?>',
		bgImage : 'files/bg.jpg',
		bgOpacity  : '<?php echo $bgOpacity/100; ?>'
	}
	
	//////////////////////////////////////////////////////////////
	/////////////////// SET UP FUNCTIONS ///////////////////////
	//////////////////////////////////////////////////////////////
		function generateTextInputBoxes(textBoxArray){
			$('.text-container').find(".toi-textline").remove();

			$.each(textBoxArray, function(index, value){
				var ipBoxName ="textLine"+index;
				var ipBox = '<div id="'+ipBoxName+'" name="'+ipBoxName+'" type="text" class="toi-textline ' +ipBoxName+'" data-number="' + index + '">' + value + '</div>';
				
				$('.text-container').append(ipBox);
			});
			
			$('.toi-textline').css({'font-size' : slideProps.textSize + 'em', 'color' : '#' + slideProps.textColour, 'font-family' : slideProps.fontFamily});
			if(slideProps.bold == "true"){			$('.toi-textline').addClass('bold');			}
			if(slideProps.shadowed == "true"){		$('.toi-textline').addClass('shadowed');		}

		}
	//////////////////////////////////////////////////////////////
	/////////////////// DOCUMENT READY ///////////////////////
	//////////////////////////////////////////////////////////////
		$(document).ready(function(){
		 //alert('asdasd');
		});

	//////////////////////////////////////////////////////////////
	/////////////////// WINDOW LOAD ///////////////////////
	//////////////////////////////////////////////////////////////
			
		$(window).on('load', function(){
		if(debug) console.log('window load');
		
		if(debug) console.log('reached');
			// get viewport size

			w = $(window).width();

			var h = window.innerHeight ? window.innerHeight : $(window).height();
			h = h -36; // hack to make up for Tim DS' hack that adds 36 pixels to hide scrollbars.
				
			dimens={
				height: h,
				width: w
			}
			
			
			var isLegacyResolution; //when true, aspect ratio is 4:3, when false, 16:9;
			
			var isPortrait = false; //default
			
			if(dimens.height > dimens.width){
				 isPortrait = true;
			}
			
			//////////////////////////////////////////////////////////////
			/////////////////// PORTRAIT DISPLAYS ///////////////////////
			//////////////////////////////////////////////////////////////
			if (isPortrait)
			{

			}
			//////////////////////////////////////////////////////////////
			//////////////////////// LANDSCAPE HD ////////////////////////
			//////////////////////////////////////////////////////////////
			else // landscape
			{
				if(dimens.width > 1000){
					isLegacyResolution = false;
					if(dimens.width < 1400)
					{
						
					}
				}
				else
				{
						// LANDSCAPE COMPOSITE (LEGACY) 
						isLegacyResolution = true;	
				}
			}
		
			generateTextInputBoxes(tbArray);
		
			// Some mathematics to space the text boxes relative to the viewport dimensions
			var verticalOffset = slideProps.verticalOffsetPercent * dimens.height;
			var verticalSpacing = slideProps.verticalSpacingPercent * dimens.height;
			
			// if(debug) console.log("verticalSpacingPercent = " + slideProps.verticalSpacingPercent);
			// if(debug) console.log("verticalSpacing = " + verticalSpacing);
			// if(debug) console.log("verticalOffset = " + verticalOffset);
			// if(debug) console.log("w = " + dimens.width);
			
			$('.toi-textline').css('margin-bottom',  verticalSpacing);
			$('.text-container').css('margin-top', verticalOffset);
			
			// the preview window when creating the TOI is 1024px wide, so we calculate
			// the relative size of the font 
			$('.text-container').css('font-size', (dimens.width/1024) * 24); 
			$('.img-container').css({
				'background-size' : dimens.width, 
				'background-image': 'url("' + slideProps.bgImage + '")',
				'opacity' : slideProps.bgOpacity
			});	
			
			$('.container').css('background-color','#' + slideProps.bgColor);
			$('.container').css('height',$(window).height());

		});
	</script>

	<style>
	
	/* SET UP BODY STYLES */
	/* These all happen in the HTML and not an external CSS because of the PHP echo'd in there */
	body{
		margin: 0; 
		padding: 0;
		/*background-color: #003366;*/
		background-color: <?php echo '#' . $bgColor; ?>;
		font-size: 22px; /* base */
		font-family: "Arial", sans-serif;
		height: 100%;
		width: 100%;
	}
	.container{	
		max-width: 100%; 
		height: 100%; 
		overflow: hidden;  
		box-sizing: border-box;
		background-size: 100% auto; 
		/*background-color: #000;*/
		position: relative;
	}
	
	.container img{width: 100%;}
	
	.text-container,.img-container{
		position: absolute; top: 0; left: 0;
		height: 100%; 
		width: 100%;
	}
	.text-container{
		z-index: 100000000000;
		font-size: 22.5px;
	}
	.img-container{
		z-index: 0;
		overflow: hidden;  
		  -webkit-background-size: 1024px auto;
		  -moz-background-size: 1024px auto;
		  -o-background-size: 1024px auto;
		background-size: 1024px auto; 
	}

	.toi-textline{
		display: block; 
		width: 100%;
		border: 0px solid red;
		position: relative;
		text-align: center;
		font-size: <?php echo $textSize . "em";?>;

		-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
		-moz-box-sizing: border-box;    /* Firefox, other Gecko */
		box-sizing: border-box;         /* Opera/IE 8+ */
	}
	
	.shadowed{text-shadow: 2px 2px 0px rgba(0, 0, 0, 1);}
	.bold{font-weight: bold;}
	</style>
</head>

<body>
	<div class="container">
		<div class="text-container">
			<!-- text boxes generated in here -->
		</div>
		<div class="img-container" style="
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $bgImage; ?>', sizingMethod='scale');
			-ms-filter: 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $bgImage; ?>', sizingMethod='scale')';">
			<!-- image background goes in here -->
		</div>
	</div>
</body>

</html>