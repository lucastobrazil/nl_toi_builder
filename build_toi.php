<?php
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////// NIGHTLIFE TOI BUILDER - 11.08.2015 - Lucas Arundell ///////////////////////////////////////
	/*

	This file is invoked during the 'build' process of a TOI slide. It basically calls up the renderer (nightlife-toi.php)
	And flattens the output to a HTML file, as well as copying fonts, css, images and generating a zip file.

	*/

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Ajax request that calls this build passes the TOI's full URL as contents variable
	//$url = isset($_POST['contents']) && ($_POST['contents'] != '') ? $_POST['contents'] : '';
	$outputFileName = isset($_POST['filename']) && ($_POST['filename'] != '') ? $_POST['filename'] : 'default';
	// For our Legacy TOI fallbacks, we won't need to copy a file, just reference the image path
	// Bool - whether or not there will be a JPEG from the server that needs to get copied into the folder
	$copy_bg_img = isset($_POST['copy_bg_img']) && ($_POST['copy_bg_img'] != '') ? $_POST['copy_bg_img'] : 'false';
	$upload_img = isset($_POST['upload_img']) && ($_POST['upload_img'] != '') ? $_POST['upload_img'] : 'false';
	$imgPath = isset($_POST['bgImage']) && ($_POST['bgImage'] != '') ? $_POST['bgImage'] : 'default_background';
	$fontName = isset($_POST['fontFamily']) && ($_POST['fontFamily'] != '') ? $_POST['fontFamily'] : 'open_sanslight';

	error_reporting(E_ALL);
	ini_set('display_errors', true);


	// ! remember to have a trailing slash for all these paths !
	define('ROOT', __dir__.'/');
	
	// where the source PHP file is
	define('INPUT_ROOT', ROOT.'source-files/nightlife-toi-renderer.php');
	
	// where the other static files are
	define('FILES_ROOT', ROOT.'source-files/');
		
	// where the source font files are (eg dir/dir/myfontnamewithoutextension)
	define('FONTS_SOURCE_FILENAME', ROOT. 'font-files/' . $fontName . '/' . $fontName);

	// where to do the local build
	define('LOCAL_ROOT', ROOT. 'output/' . $outputFileName.'.html/');
	
	// where the uploads are kept
	define('DEST_UPLOADS', LOCAL_ROOT . 'files/');
	
	// where the fonts will go
	define('FONTS_DEST_DIR', LOCAL_ROOT. 'font-files/' . $fontName . '/');

	// where the private key for xing's HDMS-LIVE access is stored
	// note that this file should be owned by nobody
	// define('HDMSLIVE_KEYFILE', ROOT.'hdms-live.key');
	$debug = 0;
	
	info('TOI Builder');
	
	function info($s) {
		global $debug;
		if($debug){
			echo "<br />", htmlspecialchars($s);
			flush();
		}
	}
	function error($s, $die=true) {
		global $debug;
		if($debug){
			echo '<br /><span style="color: red;">', htmlspecialchars($s), '</span>';

			if($die) die('</body></html>');
		}
	}
	function output_info_start() {
		global $debug;
		if($debug){
			echo '<span style="color: blue; white-space: pre;">';
		}
	}
	function output_info_end() {
		global $debug;
		if($debug){
			echo '</span>';
		}
	}
	function ok() {
		global $debug;
		if($debug){
			echo '<span class="label label-success">OK</span>';
			flush();
		}
	}
	function failed() {
		global $debug;
		if($debug){
			die('<span class="label label-danger">Error!</span>');
		}
	}
	define('NEWLINE', '<br />');
	function sys($cmd, &$ret=null) {
		output_info_start();
		$d = system($cmd.' 2>&1', $ret);
		output_info_end();
	}
	function static_file($f) {
		$fn = FILES_ROOT.$f;
		if(!file_exists($fn)) {
			trigger_error("Referenced static file $f not found!", E_USER_WARNING);
			return $f;
		}
		// append a cache buster if the file has been modified
		return $f.'?_dc='.substr(strtr(base64_encode(md5(filemtime($fn), true)), array('+'=>'_','/'=>'-')), 0, 8);
	}
	function _get_data_err($errno, $errstr, $errfile, $errline) {
		if (!(error_reporting() & $errno)) return;
		$errmap = array(
			E_ERROR => 'Error',
			E_WARNING => 'Warning',
			E_PARSE => 'Parse Error',
			E_NOTICE => 'Notice',
			E_CORE_ERROR => 'Core Error',
			E_CORE_WARNING => 'Core Warning',
			E_COMPILE_ERROR => 'Compile Error',
			E_COMPILE_WARNING => 'Compile Warning',
			E_USER_ERROR => 'User Error',
			E_USER_WARNING => 'User Warning',
			E_USER_NOTICE => 'User Notice',
			E_STRICT => 'Strict Notice',
			E_RECOVERABLE_ERROR => 'Recoverable Error',
			E_DEPRECATED => 'Deprecated Warning',
			E_USER_DEPRECATED => 'User Deprecated Warning',
		);
		$GLOBALS['_errs'][] = array(isset($errmap[$errno]) ? $errmap[$errno] : 'Unknown Error', $errstr, $errfile, $errline);
		return true;
	}
	function my_mkdir($dir){
		if(!is_dir($dir)){
			info("Creating Folder..." . $dir);
			mkdir($dir);
			chmod($dir , 0777);
			ok();
		}
	}
	function my_copy($s, $d) {
		if(!file_exists($s))
			error("File $s not found!");
		if(!copy($s, $d)) error("Failed to copy $s to $d!");
	}	
	function get_data($LOCAL_BUILD) {
		chdir(dirname(INPUT_ROOT));
		$GLOBALS['_errs'] = array();
		set_error_handler('_get_data_err');
		ob_start();
		require './'.basename(INPUT_ROOT);
		$data = ob_get_clean();
		restore_error_handler();
	
			if(!empty($GLOBALS['_errs'])) {
			foreach($GLOBALS['_errs'] as $err) {
				error("PHP $err[0]: $err[1] at $err[2]:$err[3]", false);
			}
			info(NEWLINE);
			return false;
		}
		
		// heuristic: if the end HTML tag isn't found, the page generation probably failed
		if(!strpos($data, '</html>')) {
			error('End tag not found, HTML page probably did not generate correctly.', false);
			info(NEWLINE);
			return false;
		}
		
		// minify HTML a bit
		// $data = trim($data);
		// // strip comments
		// $data = preg_replace_callback('~\<\!--(.*?)--\>~', function($m) {
		// 	if(substr($m[1], 0, 3) == '[if') // skip IE specific comments
		// 		return $m[0];
		// 	return '';
		// }, $data);
		// // minify double-spaces
		// $data = preg_replace('~ {2,}~', ' ', $data);
		// // strip newlines & tabs
		// $data = strtr($data, array("\r" => '', "\n" => '', "\t" => ''));
		
		// prepend doctype
		$data = "<!DOCTYPE html>\r\n".$data;
		return $data;
	}

	function zip_folder(){
		global $outputFileName;
		// Get real path for our folder
		$rootPath = realpath(LOCAL_ROOT);

		// Initialize archive object
		$zip = new ZipArchive();
		$zip->open(ROOT . 'output/' . $outputFileName . '.html.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
		//$zip->open(LOCAL_ROOT . $outputFileName . '.html.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(
		    new RecursiveDirectoryIterator($rootPath),
		    RecursiveIteratorIterator::LEAVES_ONLY
		);

		info('zipping folder');
		foreach ($files as $name => $file)
		{
		    // Skip directories (they would be added automatically)
		    if (!$file->isDir())
		    {
		        // Get real and relative path for current file
		        $filePath = $file->getRealPath();
		        $relativePath = substr($filePath, strlen($rootPath) + 1);

		        // Add current file to archive
		        $zip->addFile($filePath, $relativePath);
		    }
		}

		// Zip archive will be created only after closing object
		$zip->close();
	}

	function show_post_vars(){
		info("Post Variables");
		foreach ($_POST as $key => $value) {
	        info( $key );
	        info( " = " );
	        info( $value );
	        info( "<br />" );
	    }
	    info( "</p>" );
	}

	info("Generating HTML page...");
	if(!($data = get_data(true))) failed(); ok();

	// do local build first
	info("Creating Folder...");
	my_mkdir(LOCAL_ROOT);

	// Use this to do a dump of all the vars posted in submission of the form
	show_post_vars();
	
	if(!is_writable(LOCAL_ROOT))
		error("Local build dir not writable!");

	// Delete folder if it exists
	info("Clearing...");
	sys('rm -rf '.escapeshellarg(LOCAL_ROOT).'*', $rc);
	if($rc) failed(); ok();

	$g = glob(LOCAL_ROOT.'*');
	if(!empty($g))
		error("local build dir not empty!");

	// Copy files in folder
	info("Copying /files...");
	sys('cp -R -p '.escapeshellarg(FILES_ROOT).'* '.escapeshellarg(LOCAL_ROOT), $rc);

	if($rc) failed(); ok();
	
	my_mkdir(LOCAL_ROOT . 'files');
	my_mkdir(LOCAL_ROOT . 'font-files');
	my_mkdir(LOCAL_ROOT . 'font-css');
	
	if($copy_bg_img === 'true' && $upload_img === 'false'){
		info("Copying images...");
		// This stuff should be cleaned up when we no longer use the epic long url of new-devintranet etc...
		// When the actual TOI display can use relative paths, we won't need trim off all the http://new-devintra....
		// this function cuts off http:// and uses the filepath instead
		$serverPathToImg = substr_replace($imgPath, '/var/www/lucas/', 0, 46);
		
		info('Copying and renaming background image');
		my_copy($serverPathToImg, LOCAL_ROOT.'files/bg.jpg');
		ok();
		
	}else{
		info("It's an import so not copying any images, HDMS-local image path set to " . $imgPath );
	}

	if($copy_bg_img === 'true'){
		info("Copying fonts...");
		
		info($fontName);
		my_mkdir(FONTS_DEST_DIR);
		
		// Copy each font file
		foreach(array('ttf', 'woff', 'woff2', 'eot') as $extension)
	        	my_copy(FONTS_SOURCE_FILENAME . '.' . $extension, FONTS_DEST_DIR . $fontName . '.' . $extension);

		ok();

		info("Copying font CSS...");
		$fontCssLoc = ROOT . 'font-css/' . $fontName . '.css';
		my_copy($fontCssLoc, LOCAL_ROOT.'font-css/' . $fontName . '.css');
		ok();
	}else{
		info("It's an import so not copying any fonts");
	}
	info("-------------Performing upload...");
	if($upload_img === 'true'){
		info("upload_img value = " . $upload_img);
		//$target_dir = DEST_UPLOADS;
		info($_FILES["bgImage"]["name"]);
		$target_file = DEST_UPLOADS . basename($_FILES["bgImage"]["name"]);
		info('target file = '  . $target_file);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		
		// $check = getimagesize($_FILES["file"]["tmp_name"]);
		$check = getimagesize($_FILES["bgImage"]["tmp_name"]);
		if($check !== false) {
			info("File is an image - " . $check["mime"] . ".");
		$uploadOk = 1;
		} else {
			info("File is not an image.");
			$uploadOk = 0;
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    info("File already exists, overwriting.");
		    $uploadOk = 1;
		}
		// Check file size
		if ($_FILES["file"]["size"] > 5000000) {
		    info( "Sorry, your file is too large." );
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    info( "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    info("Sorry, your file was not uploaded.");
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["bgImage"]["tmp_name"], DEST_UPLOADS . 'bg.jpg')) {
		        info( "The file ". basename( $_FILES["bgImage"]["name"]). " has been uploaded.");
		    } else {
		        info( "Sorry, there was an error uploading your file.");
		    }
		}
	}

	info("Writing HTML file for local version...");

	if(!file_put_contents(LOCAL_ROOT . $outputFileName . '.html', $data)) failed();
	ok();

	info("Local version built");

	info("Setting writable permissions on local build...");
	sys('chmod -R a+w '.escapeshellarg(LOCAL_ROOT).'*', $rc);
	sys('rm '.escapeshellarg(LOCAL_ROOT). 'nightlife-toi-renderer.php', $rc);
	if($rc) failed(); ok();
	info("Build complete");
	zip_folder();


	$response = array(
		"zip_url" =>  'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}" . '/nm/nl_toi_builder/output/' . $outputFileName .'.html.zip',
		"html_url" =>  'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}" . '/nm/nl_toi_builder/output/' . $outputFileName.'.html/' . $outputFileName.'.html'
	);

	echo json_encode($response);

?>
