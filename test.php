<?php
	header("Access-Control-Allow-Origin:*");
	header('Access-Control-Allow-Methods: POST');
	
	$outputFileName = isset($_POST['filename']) && ($_POST['filename'] != '') ? $_POST['filename'] : 'crap';
	$response = array(
		// "url" => 'http:  //myhouse.com'
		"filename" => $outputFileName,
		"zip_url" =>  'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
		"html_url" =>  'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"
	);
	//$response['url'] = 'http://myhouse.com';
	// print $_GET['jsoncallback']. '('.json_encode($response).')'
	echo json_encode($response);
?>